/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  '*': 'is-logged-in',

  // Bypass the `is-logged-in` policy for:
  'entrance/*': true,
  'account/logout': true,
  'view-homepage-or-redirect': true,
  'view-faq': true,
  'view-contact': true,
  'legal/view-terms': true,
  'legal/view-privacy': true,
  'deliver-contact-form-message': true,
  'main/update-main-feed-recipes': true,
  'main/update-main-feed-foods': true,
  'recipe/show-recipe-detail': true,
  'recipe/view-recipe-detail': true,
  'food/view-food-detail': true,
  'recipe/nutrition-analysis': true,
  'recipe/next-recipe': true,
  'food/next-food': true,

};
