parasails.registerPage('homepage', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    //…
    diet: [],
    health: [],
    cuisineType: [],
    mealType: [],
    dishType: [],
    keyword: '',
    measure: '',

  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    //…
    this.recipes = this.APIRecipes.hits;
    this.currentRData = this.APIRecipes._links.next.href;
    this.currentFData = '';
    this.toggle = true;
    this.foods = {};
    this.title = 'Recipe Book';
    this.pagination = false;
    this.currentRecipes = [
      'https://api.edamam.com/api/recipes/v2/?q=&app_id=b568fee8&app_key=9548e7a9181f582d5c4d85e1a1a70656&type=public&diet=balanced'
    ];
    this.currentFoods = [];
  },
  mounted: async function(){
    this._setHeroHeight();
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    clickHeroButton: async function() {
      // Scroll to the 'get started' section:
      $('html, body').animate({
        scrollTop: this.$find('[purpose="scroll-destination"]').offset().top
      }, 500);
    },

    // Private methods not tied to a particular DOM event are prefixed with _
    _setHeroHeight: function() {
      var $hero = this.$find('[purpose="full-page-hero"]');
      var headerHeight = $('[purpose="page-header"]').outerHeight();
      var heightToSet = $(window).height();
      heightToSet = Math.max(heightToSet, 500);//« ensure min height of 500px - header height
      heightToSet = Math.min(heightToSet, 1000);//« ensure max height of 1000px - header height
      $hero.css('min-height', heightToSet - headerHeight+'px');
    },

    toggleTrue: function() {
      if (this.toggle === false) {
        this.toggle = true;
        this.title = 'Recipe Book';
        this.$forceUpdate();
      }
    },

    nextPage: async function(direction, linking){
      if (this.toggle === true){


        if (direction === 0){

          if (this.currentRecipes.length > 1){
            this.currentRData = this.currentRecipes.pop();
            linking = this.currentRecipes[this.currentRecipes.length - 1];
            let response = await Cloud.nextRecipe.with({href: linking});
            this.recipes = response.APIRecipes.hits;
            this.$forceUpdate();
          }

        } else {

          let response = await Cloud.nextRecipe.with({href: linking});
          this.recipes = response.APIRecipes.hits;
          this.currentRData = response.APIRecipes._links.next.href;
          this.$forceUpdate();
          this.currentRecipes.push(linking);

        }
      }
    },

    toggleFalse: function() {
      if (this.toggle === true) {
        this.toggle = false;
        this.title = 'Food List';
        this.$forceUpdate();
      }
    },


    goToRecipeDetail:  function(recipeUrl){
      let idStart = 38;
      let idEnd = recipeUrl.indexOf('?');
      let recipeId = recipeUrl.substring(idStart, idEnd);
      this.goto('/recipe/' + recipeId);
    },

    goToFoodDetail:  function(foodUrl){
      let mStart = this.measure.indexOf('#');
      let measure = this.measure.substring(mStart + 1);
      this.goto('/food/' + foodUrl + '/' + measure);
    },

    goToCustomAnalysis:  function(){
      this.goto('/nutrition/custom-analysis');
    },


    updateFoodFeed: async function(){

      if (this.keyword.length > 0) {
        let response = await Cloud.updateMainFeedFoods.with({keyword: this.keyword});
        this.foods = response.APIFoods.hints;
        this.currentFData = response.APIFoods._links.next.href;
        this.currentFoods.push(response.APIRequest);
        this.$forceUpdate();
      }
    },

    updateRecipeFeed: async function(){

      if (this.diet.length        > 0 ||
          this.health.length      > 0 ||
          this.cuisineType.length > 0 ||
          this.mealType.length    > 0 ||
          this.dishType.length    > 0 )
      {
        let response = await Cloud.updateMainFeedRecipes.with(
        { diet: this.diet, health: this.health, cuisineType: this.cuisineType,
          mealType: this.mealType, dishType: this.dishType}
        );
        this.recipes = response.APIRecipes.hits;
        this.currentRecipes = [];
        this.currentRData = response.APIRecipes._links.next.href;
        this.currentRecipes.push(response.APIRequest);
        this.$forceUpdate();
      }

    },

  }
});
