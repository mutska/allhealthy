parasails.registerPage('recipe-detail', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    //…
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    //…
    this.recipe = this.APIRecipe.recipe;
    this.nutrition = {};
    this.info = '';
    this.quantity = '';
    this.show = false;
  },
  mounted: async function() {
    //…
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    //…
    getNutritionAnalysis: async function(){
      let ingr = this.recipe.ingredientLines;
      console.log('getting nutrition');
      console.log(ingr);
      let response = await Cloud.nutritionAnalysis.with({ingredients: ingr});
      
      if (this.show) {
        this.show = false;
        this.nutrition = '';
        this.info = '';
        this.quantity = '';
      } else {
        this.nutrition = response.APINutrition;
        this.info = 'Information';
        this.quantity = 'Quantity';
        this.show = true;
      }

      console.log(response);
      this.$forceUpdate();
    },
  }
});
