# Proyecto Interfaces de Usuario
Repositorio para el Proyecto del Curso de Interfaces de Usuario

## Content

- [Integrantes](#Integrantes)
- [Requisitos](#Requisitos)
- [Ejecutar el proyecto](#Instrucciones)

## Integrantes

| Nombre                         | Número de Cuenta |
|--------------------------------|------------------|
| Oscar Fernando Millan Pimentel | 315067998        |
| Marco Antonio Velasco Flores   | 418004087        |


## Requisitos

Necesariamente se necesita instalar previamente `Node Js` en el equipo, antes de
continuar con las instrucciones.

| Tecnologia | Versión                                                             |
|------------|---------------------------------------------------------------------|
| Node Js    | [`16.13.1`](https://nodejs.org/en/blog/release/v16.13.1/)           |
| Sails Js   | [`1.5.0`](https://github.com/balderdashy/sails/releases/tag/v1.5.0) |
| NPM        | [`8.3.0`](https://www.npmjs.com/package/npm/v/8.3.0 )               |


## Instrucciones

1. Clonar el repositorio

> $ git clone https://gitlab.com/mutska/allhealthy.git

2. Moverse a la dirección del proyecto

> $ cd allhealthy

3. Instalar las dependencias requeridas

> $ npm ci

4. Dentro del proyecto vamos a correr el `cli` de `sails` con:

> $ sails lift

5. Ingresar a la dirección a la aplicación [AllHealthy](http://localhost:1337)

6. Recomendaciones

> El framework de Sails.js tarda en cargar todos los recursos, recomendamos esperar un minuto antes
de abrir la aplicación en el navegador

> Si da error recargar la página o dirigirse a [AllHealthy](http://localhost:1337), en el peor caso
reiniciar el proyecto con `sails lift`.

7. Usuario default

Para acceder al análisis nutricional personalizado iniciar sesión con las siguientes credenciales:

> user: admin@example.com

> password: abc123


### all-healthy

a [Sails v1](https://sailsjs.com) application


### Links

+ [Sails framework documentation](https://sailsjs.com/get-started)
+ [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)
+ [Professional / enterprise options](https://sailsjs.com/enterprise)


### Version info

This app was originally generated on Tue Jan 04 2022 18:16:29 GMT-0600 (hora estándar central) using Sails v1.5.0.




