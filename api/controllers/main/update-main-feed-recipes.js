module.exports = {


  friendlyName: 'Update main feed recipes',


  description: 'Refresh the main feed according with user inputs',


  inputs: {

    diet: {
      type: 'json'
    },

    health: {
      type: 'json'
    },

    cuisineType: {
      type: 'json'
    },

    mealType: {
      type: 'json'
    },

    dishType: {
      type: 'json'
    },


  },


  exits: {

  },


  fn: async function (inputs) {

    var APIRequest = 'https://api.edamam.com/api/recipes/v2/?q=&app_id=b568fee8&app_key=9548e7a9181f582d5c4d85e1a1a70656&type=public';

    // Diet Parameters
    let dietParameters = Object.values(inputs.diet).map(p => p.concat('&'));
    var diet = '';
    if (dietParameters.length > 0) {
      diet = diet.concat('&diet=');
      diet = diet.concat(dietParameters.join(''));
      diet = diet.slice(0, -1);
    }


    // Health Parameters
    let healthParameters = Object.values(inputs.health).map(p => p.concat('&'));
    var health = '';
    if (healthParameters.length > 0) {
      health = health.concat('&health=');
      health = health.concat(healthParameters.join(''));
      health = health.slice(0, -1);
    }

    // Cuisine Type Parameters
    let cuisineTypeParameters = Object.values(inputs.cuisineType).map(p => p.concat('&'));
    var cuisineType = '';
    if (cuisineTypeParameters.length > 0) {
      cuisineType = cuisineType.concat('&cuisineType=');
      cuisineType = cuisineType.concat(cuisineTypeParameters.join(''));
      cuisineType = cuisineType.slice(0, -1);
    }

    // Meal Type Parameters
    let mealTypeParameters = Object.values(inputs.mealType).map(p => p.concat('&'));
    var mealType = '';
    if (mealTypeParameters.length > 0) {
      mealType = mealType.concat('&mealType=');
      mealType = mealType.concat(mealTypeParameters.join(''));
      mealType = mealType.slice(0, -1);
    }

    // Dish Type Parameters
    let dishTypeParameters = Object.values(inputs.dishType).map(p => p.concat('&'));
    var dishType = '';
    if (dishTypeParameters.length > 0) {
      dishType = dishType.concat('&dishType=');
      dishType = dishType.concat(dishTypeParameters.join(''));
      dishType = dishType.slice(0, -1);
    }

    APIRequest = APIRequest.concat(diet, health, cuisineType, mealType, dishType);

    let bent = require('bent');
    let getJSON = bent('json');
    var APIRecipes = await getJSON(APIRequest);
    return {APIRecipes, APIRequest};
  }


};
