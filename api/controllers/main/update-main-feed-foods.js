module.exports = {


  friendlyName: 'Update main feed foods',


  description: 'Refresh the main feed according with user inputs',


  inputs: {
    keyword: {
      type: 'string'
    },
  },


  exits: {

  },


  fn: async function (inputs) {

    var APIRequest = 'https://api.edamam.com/api/food-database/v2/parser?'
    
    // keyword Parameter
    let keywordParameter = inputs.keyword;
    var ingr  = '';
    if (keywordParameter.length > 0) {
      ingr = ingr.concat('ingr=');
      ingr = ingr.concat(keywordParameter);
    }

    APIRequest = APIRequest.concat(ingr);
    APIRequest = APIRequest.concat('&category=generic-foods&app_id=e77ac92f&app_key=dbc33a80fcdf61d22bb892b137126a69');
    let bent = require('bent');
    let getJSON = bent('json');
    var APIFoods = await getJSON(APIRequest);
    return {APIFoods};
  }


};
