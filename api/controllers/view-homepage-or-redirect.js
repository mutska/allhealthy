const { get } = require('grunt');

module.exports = {


  friendlyName: 'View homepage or redirect',


  description: 'Display or redirect to the appropriate homepage, depending on login status.',


  exits: {

    success: {
      statusCode: 200,
      description: 'Requesting user is a guest, so show the public landing page.',
      viewTemplatePath: 'pages/homepage'
    },

    redirect: {
      responseType: 'redirect',
      description: 'Requesting user is logged in, so redirect to the internal welcome page.'
    },

  },


  fn: async function () {

    let bent = require('bent');
    let getJSON = bent('json');
    var APIRecipes = await getJSON('https://api.edamam.com/api/recipes/v2/?q=&app_id=b568fee8&app_key=9548e7a9181f582d5c4d85e1a1a70656&type=public&diet=balanced');
    return {APIRecipes};

  }


};
