module.exports = {


  friendlyName: 'View recipe detail',


  description: 'Display "Recipe detail" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/recipe/recipe-detail'
    }

  },


  fn: async function () {

    // Respond with view.
    let recipeId = this.req.param('recipeId');
    var APIRequest = 'https://api.edamam.com/api/recipes/v2/';
    APIRequest = APIRequest.concat(recipeId);
    APIRequest = APIRequest.concat('?type=public&app_id=b568fee8&app_key=9548e7a9181f582d5c4d85e1a1a70656');
    let bent = require('bent');
    let getJSON = bent('json');
    let APIRecipe = await getJSON(APIRequest);
    return {APIRecipe};
  }


};
