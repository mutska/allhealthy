module.exports = {


  friendlyName: 'Next recipe',


  description: '',


  inputs: {

    href: {
      type: 'string'
    },

  },


  exits: {

  },


  fn: async function (inputs) {

    // All done.
    let next = inputs.href;
    let bent = require('bent');
    let getJSON = bent('json');
    var APIRecipes = await getJSON(next);
    return {APIRecipes};

  }


};
