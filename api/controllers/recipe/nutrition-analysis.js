module.exports = {


  friendlyName: 'Nutrition analysis',


  description: '',


  inputs: {

    ingredients: {
      type: 'json'
    },

  },


  exits: {

  },


  fn: async function (inputs) {

    // All done.
    var APIRequest = 'https://api.edamam.com/api/nutrition-details?app_id=9464e986&app_key=fdbe8095bdb43d21ecddae518b248a1b';
    let ingredients = inputs.ingredients;
    let bent = require('bent');
    let post = bent(APIRequest, 'POST', 'json', 200);
    let APINutrition = await post('', {title: 'nutrition analysis request', ingr: ingredients});
    return {APINutrition};
  }


};
