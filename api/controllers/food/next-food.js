module.exports = {


  friendlyName: 'Next food',


  description: '',


  inputs: {

    href: {
      type: 'string'
    },

  },


  exits: {

  },


  fn: async function (inputs) {

    // All done.
    let next = inputs.href;
    let bent = require('bent');
    let getJSON = bent('json');
    var APIFoods = await getJSON(next);
    return {APIFoods};

  }


};
