module.exports = {


  friendlyName: 'View food detail',


  description: 'Display "Food detail" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/food/food-detail'
    }

  },


  fn: async function () {

    // Respond with view.
    let fId = this.req.param('foodId');
    let measure = this.req.param('measure');
    var APIRequest = 'https://api.edamam.com/api/food-database/v2/nutrients?app_id=e77ac92f&app_key=dbc33a80fcdf61d22bb892b137126a69';

    var mURI = 'http://www.edamam.com/ontologies/edamam.owl#';
    mURI = mURI.concat(measure);

    let bent = require('bent');
    let post = bent(APIRequest, 'POST', 'json', 200);
    let APIFood = await post('', {ingredients: [{quantity: 1, measureURI: mURI, foodId: fId}]});
    return {APIFood};
  }

};
