module.exports = {


  friendlyName: 'View custom nutrition analysis',


  description: 'Display "Custom nutrition analysis" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/nutrition/custom-nutrition-analysis'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
